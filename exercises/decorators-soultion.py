from functools import wraps

# Exercise 1

def debug(func):
    @wraps(func):
    def wrapper(*args, **kwargs):
        print(func.__name__)
        print(args)
        print(kwargs)
        return func(*args, **kwargs)

    return wrapper

# Exercise 2

def debug(print_func):
    def outer_wrapper(func):
        @wraps(func):
        def wrapper(*args, **kwargs):
            print_func(func.__name__)
            print_func(args)
            print_func(kwargs)
            return func(*args, **kwargs)
        return wrapper
    return outer_wrapper

# Exercise 3

from collections import defaultdict

class EventBus:
    def __init__(self):
        self._handlers = defaultdict(list)

    def onevent(self, event_name):
        def _inner_onevent(func):
            self._handlers[event_name].append(func)
            return func

        return _inner_onevent

    def publish(self, event_name, event_data):
        for handler in self._handlers.get(event_name, []):
            handler(event_name, event_data)
