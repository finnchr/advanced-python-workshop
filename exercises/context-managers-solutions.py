# Context managers

class AllocateResource:
    reference_count = 0

    def __init__(self):
        AllocateResource.reference_count += 1
        self._initialized = True

    def close(self):
        self._initialized = False
        AllocateResource.reference_count -= 1

    def dostuff(self):
        if not self._initialized:
            raise Error("Resouce not initialized")


# Exercise 1
import contextlib

@contextlib.contextmanager
def get_resource():
    resource = Resource()
    try:
        yield resource
    finally:
        resource.close()


def test_exercise_1():
    refcount = AllocateResource.reference_count

    with get_resource() as resource:
        resource.dostuff()
        assert AllocateResource.reference_count == refcount+1

    assert AllocateResource.reference_count == refcount

# Exercise 2
from contextlib import closing

def test_exerxise_2():
    refcount = AllocateResource.reference_count

    with closing(AllocateResource()) as resource:
        resource.dostuff()
        assert AllocateResource.reference_count == refcount+1

    assert AllocateResource.reference_count == refcount
