# Additional exercises

# Exercise 1

def union(left, right):
    return sorted(set(a+b))

def test_union():
    a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    b = [1, 2, 3, 4, 5, 6, 7, 8, 21]
    c = union(a, b)
    assert c == [1,2,3,4,5,6,7,8,13,21,34,55,89]


# Exercise 2

from functools import lru_cache

@lru_cache(100)
def fib(n):
    if n <= 0:
       return 0
    elif n == 1 or n == 2:
        return 1
    else:
        return fib(n-2) + fib(n-1)

def test_fibonacci_1():
    assert fib(0) == 0
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(3) == 2
    assert fib(4) == 3
    assert fib(5) == 5

# Alternative solution

class Fibonacci:
    def __init__(self):
        self._cache = [0, 1, 1]

    def __call__(self, n):
        if len(self._cache) > n:
            return self._cache[n]
        else:
            self._cache.append(self(n-2) + self(n-1))

        return self(n)

def test_fibonacci_2():
    fib = Fibonacci()
    assert fib(0) == 0
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(3) == 2
    assert fib(4) == 3
    assert fib(5) == 4

# Exercise 3

def test_sum_fibonacci_numbers_1():
    assert sum([fib(n) for n in range(0, 10)]) == 88
    assert sum([fib(n) for n in range(0, 20)]) == 10945
    assert sum([fib(n) for n in range(0, 30)]) == 1346268

from functools import reduce

def test_sum_fibonacci_numbers_2():
    reduce(lambda a,b: a+b, [fib(n) for n in range(0, 10)]) == 88
    reduce(lambda a,b: a+b, [fib(n) for n in range(0, 20)]) == 10945
    reduce(lambda a,b: a+b, [fib(n) for n in range(0, 30)]) == 1346268

# Exercise 4

def test_split_list():
    numbers_part1 = [1, 9, 2, 8, 3]
    numbers_part2 = [7, 4, 6, 5, 0]
    numbers = numbers_part1 +  numbers_part2

    index = round(len(numbers)/2)
    assert index == 5

    first_half = sum(numbers[:index])
    second_half = sum(numbers[index:])

    assert first_half+second_half == sum(numbers)
    assert first_half == sum(numbers_part1)
    assert second_half == sum(numbers_part2)

# Exercise 5, 6 and 7

class Vector:
    def __init__(self, x, y, z):
        self._vec = [x, y, z]

    @property
    def x(self):
        return self._vec[0]

    @property
    def y(self):
        return self._vec[1]

    @property
    def z(self):
        return self._vec[2]

    def __add__(self, right):
        if isinstance(right, Vector):
            return Vector(
              self.x + right.x,
              self.y + right.y,
              self.z + right.z
            )
        else:
            raise Exception()

    def __mul__(self, right):
        if isinstance(right, Vector):
            return Vector(
                self.y*right.z - self.z*right.y,
                self.z*right.x - self.x*right.z,
                self.x*right.y - self.y*right.x
            )
        elif isinstance(right, int) or isinstance(right, float):
            return Vector(
                self.x*right,
                self.y*right,
                self.z*right
            )
        else:
            raise Exception()



def test_vector_add():
    a = Vector(3,6,7)
    b = Vector(2,11,13)
    c = a + b

    assert c.x == a.x + b.x
    assert c.y == a.y + b.y
    assert c.z == a.z + b.z

def test_vector_cross_product():
    a = Vector(1,2,3)
    b = Vector(-2,9,7)
    c = a * b

    assert c.x == a.y*b.z - a.z*b.y
    assert c.y == a.z*b.x - a.x*b.z
    assert c.z == a.x*b.y - a.y*b.x

def test_vector_scalar():
    a = Vector(3,5,9)
    c = a * 3

    assert c.x == a.x * 3
    assert c.y == a.y * 3
    assert c.z == a.z * 3


# Exercise 8

import json

with open("people.json", "w") as jsonfile:
    jsonfile.write(json.dumps(list(get_values()), indent=4))


## Exercise 9

import json

with open("people.json", "r") as fh:
    people = json.load(fh)
    print("\n".join([p["name"] for p in people if int(p["age"]) > 18]))

# Exercise 10

def people_hook(p):
    return Person(p["name"], int(p["age"]))

with open("people.json", "r") as fh:
    people = json.load(fh, object_hook=people_hook)
    print(people)
    print("\n".join([p.name for p in people if p.age > 18]))
