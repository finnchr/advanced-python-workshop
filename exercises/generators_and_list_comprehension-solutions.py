numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]


def multiply_with_ten():
    return [n*10 for n in numbers]

def map_to_true_false():
    return [n%2==0 for n in numbers]

def keep_even_numbers():
    return [n for n in numbers if n%2==0]

def test_generators_and_list_comprehension_exercise_1():
    result = multiply_with_ten()

    for index,value in enumerate(numbers):
        assert value*10 == result[index]

def test_generators_and_list_comprehension_exercise_2()
    result = map_to_true_false()

    for index,value in enumerate(numbers):
        assert (value%2 == 0) == result[index]

def test_generators_and_list_comprehension_exercise_3()
    result = keep_even_numbers()

    for n in result:
        assert n%2 == 0
