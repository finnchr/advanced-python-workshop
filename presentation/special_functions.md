# Special functions and attributes

Et utvalg

* `__init__` - konstruktør
* `__del__` - finalizer (ikke destruktør) - slå opp
* `__repr__` - "offisiell" string-representasjon av objekt
* `__str__` - "pen" string-representasjon av objekt
* `__enter__` - starter context
* `__exit__` - avslutter context
* `__name__` - funksjonsnavn
* `__bool__` - True/False test av object
* `__call__` - bruk instans som funksjon
* `__iter__` - iterator
* `__eq__` - x == y
* `__add__` - x + y
* `__mul__` - x * y

Note:
* Noen av disse har vi sett på, som f.eks. `__enter__` og `__exit__`
* Det finnes mange flere enn de som er nevnt her. Se i dokumentasjonen for flere ...

---

## `__call__`

```
import sys

class Printer:
    def __init__(self, fh):
        self._fh = fh

    def __call__(self, txt):
        print(txt, file=self._fh)

p = Printer(sys.stderr)
p("Foo")
```

Note:
Instans av typen printer er callable, og vi bruker instansen som en funksjon.

---

## `__bool__`

```
class Valid:
    def __init__(self, value):
        self._value = value

    def __bool__(self):
        return self._value == "1"

v = Valid("1")

if v:
    do_something()
```

Note:
Vi definerer om et objekt blir evaluert som True eller False.

F.eks. evalueres en tom liste til False, og vi kan skrive penere uttrykk enn `if len(my_list) > 0`

---

## `__getitem__`

```
class Row:
    def __init__(self, row_as_dict, default_values=None):
        self._row_as_dict = row_as_dict

    def __getattr__(self, name):
        if name in row_as_dict:
            return self._row_as_dict[name]
        elif default_values and name in default_values:
            return default_values[name]
        else:
            raise Exception("Unknown column name {0}".format(name))


def get_rows():
    with get_connection() as c:
        for row_as_dict in c.query("select id,name from people"):
            yield Row(row_as_dict)


for row in get_rows():
    print(row.id, row.name)
```

Note:
Gir oss muligheten til å implementere hvordan oppslag på attributt skal skje. I eksempel kan vi gjøre oppslag på verdier i en rad, med mulighet til å angi default-verdier som manglede attributter.
