# Tools

* Makefiles
* Lint
* Pytest
* PyCharm
* Virtualenv
* PIP

---

## Makefiles

Samling av regler som angir hvordan noe skal bygges. Historisk sett et verktøy for C/C++

<br/>

* Støtte på alle platformer
* Lett å komme igang

Note:
Se på Makefile som hører til presentasjon (for de som er interessert)

---

## Eksempel

```
VENV=.venv
PIP=$(VENV)/bin/pip
ENV=dev
REQUIREMENTS=requirements-$(ENV).txt

clean:
	rm -rf $(VENV)

$(VENV):
	virtualenv --python=python3 $(VENV)

bootstrap: $(VENV)
	$(PIP) install -r $(REQUIREMENTS)

test:
	pytest --verbosity=1
```

```
$ make bootstrap
```

Note:
Targets A har avhengigheter på target B. Når B er oppfylt så kan A kjøre.

---

## Lint

* Pylint
* Flake8
* coverage.py

Note:
Velg èn

---

## PyCharm

IDE for Python. Støtte for flere rammeverk, bl.a. testing.
