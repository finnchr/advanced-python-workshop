# Pythonic

> Exploiting the features of the Python language to produce code that is clear, concise and maintainable.

> Pythonic means code that doesn't just get the syntax right but that follows the conventions of the Python community and uses the language in the way it is intended to be used.

---

Ikke bra ...

```
i = 0
while i < len(values):
    do_something(values[i])
    i += 1
```

... bra

```
for x in values:
    do_something(x)
```

---

Virkelig ikke bra ...

```
cats = []
for i in range(0, len(animals)):
    if animals[i].type_ == "cat":
        cats.append(animals[i])
return cats
```

litt bedre ...

```
for i in range(0, len(animals)):
    if animals[i].type_ == "cat":
        yield animals[i]
```

... best

```
cats = [e for e in animals if e.type_ == "cat"]
```
