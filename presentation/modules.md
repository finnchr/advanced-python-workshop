# Moduler

* Python-kode er organisert i moduler
* Kan inneholder funksjoner, klasser og variabler/konstanter
* Moduler kan importere andre moduler
* `<module name>/__init__.py`

Note:
Python gjenkjenner moduler vha. filen `__init__.py` som ligger på root i mappen som utgjør modulen. `__init__.py` initialiserer modulen.

---

## Eksempel

`__init__.py`

```
worlddomination
└─ europa
   └─ norway
      └─ __init__.py
```

```
CAPITAL="Oslo"
CURRENCY="NOK"

from worlddomination.europa.norway.cities import oslo as capital

def format_date(dt):
    raise NotImplementedError
```

---

... bruk av modulen

```
import worlddomination.europa.norway as norway
from worlddomination.europa.norway import capital

import unittest

class ModuleTestCase(unittest.TestCase):
    def test_that_CAPITAL_equals_to_oslo(self):
        self.assertEqual(norway.CAPITAL, "Oslo")
        self.assertEqual(norway.CURRENCY, "NOK")

    def test_capital_name_equals_to_oslo(self):
        self.assertEqual(capital.name, norway.CAPITAL)

    def test_format_date(self):
        self.assertRaises(NotImplementedError, norway.format_date(None))
```

---

## Kjør modul

`__main__.py` definerer hvordan en modul skal kjøres

<br/>

```
worlddomination
└─ __main__.py
```

Innhold

```
import worlddomination.europa.norway
import worlddomination.europa.sweden
import worlddomination.asia.japan
import worlddomination.asia.china
...

worlddomination.europa.norway.dominate()
worlddomination.europa.sweden.dominate()
worlddomination.asia.japan.dominate()
worlddomination.asia.china.dominate()
...
```

Syntax for å kjøre modul

```
$ python -m worlddomination
```
