# Generators

> Generator functions allow you to declare a function that behaves like an iterator, i.e. it can be used in a for loop.

---

## Iterator - den tradisjonelle måten

```
class Range(object):
    def __init__(self, start, end):
        self._cur = start - 1
        self._end = end

    def __iter__(self):
        return self

    # Python 3 compatibility
    def __next__(self):
        return self.next()

    def next(self):
        if self._cur + 1 < self._end:
            self._cur += 1
            return self._cur
        else:
            raise StopIteration()
```

---

## Generator

```
def range(start, end):
    cur = start
    while cur < end:
        yield cur
        cur += 1
```

(merk at `range` er en innebygget funksjon i Python)

Det er ikke noe i veien for at en generator kan bruke `yield` flere ganger

```
def numbers(start, end):
    cur = start
    while cur < end:
        yield cur
        cur += 1
        yield cur * 2
        yield cur * 3
        yield cur * 4
        yield cur * 5
```

---

## Generator expressions / List comprehension

> Generator expressions provide an additional shortcut to build generators out of expressions similar to that of list comprehensions.

> We can think of list comprehensions as generator expressions wrapped in a list constructor.

---

### Generator expression

```
doubles = list(2*n for n in range(0, 10))
```

### List comprehension

```
doubles = [2 * n for n in range(0, 10)]
```

Note:
* Øverst lager vi en liste med verdi kalkulert fra uttrykket
* Nederst lager vi en generator, som genererer neste verdi når vi ber om den

---

## Hvorfor bruke generators/list comprehension?

* Generere verdi når den blir etterspurt / lazy / on demand
* Trenger ikke vente til alle verdier er generert
* Enkelt (sammenlignet med iterator-klasse)
* Lettlest og elegant kode

---

## Eksempel

```
selected_numbers = [n for n in selected_numbers if n > MIN]
selected_numbers = [n for n in selected_numbers if n < MAX]
selected_numbers = [n for n in selected_numbers if check_whatever(n)]
selected_numbers = [pow(n, 2) for n in selected_numbers]
```

---

## Et litt mer avansert eksempel

```
x = [1, 2, 3]
y = [5, 10, 15]
allproducts = [a*b for a in x for b in y]
```
