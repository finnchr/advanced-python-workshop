# Testing

* unittest
* pytest

Note:
Jeg antar at dere har erfaring med enhetstesting - og jeg forklarer ikke hva det er.

---

# unittest

En del av standardbiblioteket - ingen eksterne avhengigheter

---

## Eksempel

```
import unittest

class MyTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @unittest.skip("demonstrating skipping")
    def test_nothing(self):
        self.fail("shouldn't happen")

    def test_exception(self):
        with self.assertRaises(Exception):
            raise Exception()

    def test_upper(self):
        self.assertEqual("Python".upper(), "PYTHON")

    def test_lower(self):
        self.assertNotEqual("Python".lower(), "PYTHON")


if __name__ == '__main__':
    unittest.main(verbosity=2)
```

Note:
Test-metoder må ligge i TestCase-klasse

---

# pytest

> The pytest framework makes it easy to write small tests, yet scales to support complex functional testing for applications and libraries.

* Trenger ikke TestCase-klasse
* En fuksjon som starter med `test_` er en test
* Bruker `assert` (f.eks. `assert x == y`)
* Gir god tilbakemelding på hva hvor tester feiler
* Enkel test discovery; leser `**/test_*.py`
* Kjører også tester skrevet med unittest

---

## Eksempel

```
import pytest

def test_something():
    assert str(1) == "1"

@pytest.mark.skip(reason="No reason")
def test_skipped():
    assert 2+2 == 5

def test_expect_exception():
    with pytest.raises(Exception):
        raise Exception()
```

Note:
Kan lage test-metoder som ikke tilhører noen klasse.
Støtter bruk av assert
