## Multiprocessing vs Asyncio

<pre>
CPU Bound => Multi Processing<br />
I/O Bound, Fast I/O, Limited Number of Connections => Multi Threading<br />
I/O Bound, Slow I/O, Many connections => Asyncio
</pre>

Note:
Jeg demonstrer ikke multi threading, men det er ganske likt som multi processing.
