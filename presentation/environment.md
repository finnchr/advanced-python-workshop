# Miljø

* Isolert miljø
* Unngå global installering av biblioteker
* Utvikling og produksjon er ikke like

Note:
* Vi ønsker et isolert miljø hvor vi kan installere biblioteker, verktøy mm.

* Ønsker ikke å installere biblioteker globalt - ulike applikasjoner bruker ikke nødvendigvis
samme versjon av et bibliotek.

* I utviklingsmiljøet ønsker vi typisk å installere verktøy som som ikke skal være med i
produksjonsmiljø.

---

## Virtualenv

Lager isolert Python-miljø hvor biblioteker kan installeres uten at det påvirker global installasjon og andre Python-applikasjoner.

<br/>

Lager nytt miljø og aktiverer det
```
$ virtualenv --python=python3 .venv
$ source .venv/bin/activate
```

Deaktiverer miljøet
```
$ deactivate
```

---

## PIP

Python package manager. Installerer og avinstallerer biblioteker og avhengigheter.</p>

<br/>

Installere bibliotek
```
pip install requests
```

<p class="left">Installere biblioteker angitt i fil</p>
```
pip install -r requirements.txt
```

---

## pipenv

Pakker `virtualenv` og `pip` inn i ett verktøy (og mer).

Note:
Jeg har veldig begrenset erfaring med pipenv, men det ser ut til å vinne popularitet i miljøet.
