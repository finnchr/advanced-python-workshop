# PEP8

Style Guide for Python Code

* Use 4 __spaces__ per indentation level
* Code in the core Python distribution should always use UTF-8
* Imports should usually be on separate lines
* `single_trailing_underscore_`: used by convention to avoid conflicts with Python keyword
* Modules and packages should have short, all-lowercase names. Underscores can be used in the module name if it improves readability
* Class names should normally use the CapWords convention
* Function names/variable names should be lowercase, with words separated by underscores as necessary to improve readability

Note:
Alle bør ta en titt på style guide'en!
