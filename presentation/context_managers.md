# Context managers

Allokere og frigjøre ressurser.

## Eksempel

```
def read_lines(filename):
    with open(filename, "r") as fh:
        for line in fh:
            yield line
```

... erstatter

```
def read_lines(filename):
    fh = open(filename, "r")
    try:
        for line in fh:
            yield line
    finally:
        fh.close()
```

Note:
Conext manager sørger for at filen lukkes "automatisk" når vi er ferdig å lese den.

---

## Under the hood

```
class open_file:
    def __init__(self, filename):
        self._filename = filename
        self._handle = None
        pass

    def __enter__(self):
        self._handle = open(self._filename, "r")
        return self._handle

    def __exit__(self, exc_type, exc_value, traceback):
        self._handle.close()
        pass

with open_file("foo.txt") as fh:
    fh.write("bar")
```

Note:
`__enter__` åpner context og `__exit__` lukker den.

---

## contextlib og contextmanager

```
import contextlib

@contextlib.contextmanager
def open_file(filename):
    fh = open(filename, "r")
    try:
        yield fh
    finally:
        fh.close()
```

Note:
Koden over er en del enklere enn forrige eksempel hvor vi implementerte `__enter__` og `__exit__` selv.

---

## Closing

Returnerer en context manager som lukker ressurs når with-blokk er ferdig.

Note:
Forutsetter at ressurs har metode `close`

---

### Eksempel

```
from contextlib import closing
from urllib.request import urlopen

with closing(urlopen('http://www.python.org')) as page:
    for line in page:
        print(line)
```

... erstatter

```
page = urlopen('http://www.python.org')
try:
    for line in page:
        print(line)
finally:
    page.close()
```

---

## Suppress exceptions

Returerer context manager som ignorerer angitte exceptions.

Note:
Stopper exception - i praksis det samme som en tom catch-blokk

---

### Eksempel

```
from contextlib import suppress

with suppress(FileNotFoundError):
    os.remove('somefile.tmp')
```

... erstatter

```
try:
    os.remove("somefile.tmp")
execpt FileNotFoundError:
    pass
```

---

## Context decorator

Klasse som gjøre det mulig å bruke context manager som decorator.

---

### Eksempel

```
from contextlib import ContextDecorator
import logging

logging.basicConfig(level=logging.INFO)

class track_entry_and_exit(ContextDecorator):
    def __init__(self, name):
        self.name = name

    def __enter__(self):
        logging.info('Entering: %s', self.name)

    def __exit__(self, exc_type, exc, exc_tb):
        logging.info('Exiting: %s', self.name)
```
... brukes slik

```
with track_entry_and_exit('widget loader'):
    print('Some time consuming activity goes here')
    load_widget()
```

... eller slik

```
@track_entry_and_exit('widget loader')
def activity():
    print('Some time consuming activity goes here')
    load_widget()
```



Note:
Det viktige her er at vi arver fra ContextDecorator.
