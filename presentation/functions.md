# Funksjoner

```
def hello_world():
    print("Hello world!")
```

Note:
Vi ser litt næmere på fuksjoner

---

## Tildele funksjon til variabel

```
>>> hello = hello_world()
>>> hello()
Hello world!
```

Note:
En funksjonsreferanse kan tildeles variabel

---

## Motta funksjon som arguement
```
def hello(func):
    func()
    print("Hei verden!")

>>> hello(hello_world)
Hello world!
Hei verden!
```

Note:
Funksjonsreferanser kan brukes som funksjonsargument

---

## Definere funksjon inni annen funksjon
```
def create_hello_world(print_func):
    def _hello_world():
        print_func("Hello world!")
    return _hello_world

>>> hello_world = create_hello_world(print)
>>> hello_world()
Hello world!
```

Note:
En funksjon kan defineres inni en annen funksjon. Samme som closure i javascript?

---

## Vilkårlige argumenter

```
def my_func(*args, **kwargs):
	pass
```

Note:
- `*args` - posisjonelle argumenter (tuple)
- `**kwargs` - navngitte argumenter (dict)

---

## Eksempel

```
def my_func(*args, **kwargs):
	print("type(args) = {0}".format(type(args)))
	print("args = {0}".format(args))
	print("type(kwargs) = {0}".format(type(kwargs)))
	print("kwargs = {0}".format(kwargs))

>>> my_func(1,2,3)
type(args) = <class 'tuple'>
args = (1, 2, 3)
type(kwargs) = <class 'dict'>
kwargs = {}

>>> my_func(a=4, b=5, c=6)
type(args) = <class 'tuple'>
args = ()
type(kwargs) = <class 'dict'>
kwargs = {'a': 4, 'b': 5, 'c': 6}
```

---

## Det er også mulig å kombinere:

```
def my_func(x, y, **kwargs):
	print("x = {0}".format(x))
	print("y = {0}".format(y))
	print("type(kwargs) = {0}".format(type(kwargs)))
	print("kwargs = {0}".format(kwargs))

>>> my_func(1, 2, a=4, b=5, c=6)
x = 1
y = 2
type(kwargs) = <class 'dict'>
kwargs = {'a': 4, 'b': 5, 'c': 6}
```

Note:
De 2 første argumentene er posisjonelle, de 3 siste er navngitte argumenter.

---

## Praktiske eksempel

```
def max(*args):
	if len(args) == 0:
		raise Exception("Expected a least one argument")
	if len(args) == 1:
		return args[0]
	elif len(args) == 2:
		return args[0] if args[0] > args[1] else args[1]
	else:
		return max(args[0], max(*args[1:]))

>>> max(8, 6, 4, 2, 1, 3, 5, 7, 9)
9
```
