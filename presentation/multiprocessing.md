# Multiprocessing

Tråder er ikke alltid optimalt i Python pga. "global interpreter lock" - derfor kan multiprocessing være fortrukket måte å gjøre parallelprosessering. Men det kan være tungt å kommunisere mellom prosesser.

Note:
Global interpreter lock fører til at
* èn tråd kjører om gangen
* Python interpreter switcher mellom tråder for å samtidighet

---

## Eksempel

```
from multiprocessing import Pool, TimeoutError
import time
import os

def f(x):
    return x*x

if __name__ == '__main__':
    # start 4 worker processes
    with Pool(processes=4) as pool:
        res = pool.map(f, range(10))
        print(res)

    with Pool(processes=4) as pool:
        res = pool.map_async(f, range(10))
        print(res.get(timeout=10))

    with Pool(processes=4) as pool:
        res1 = pool.apply_async(f, (1000,))
        res2 = pool.apply_async(f, (1000,))
        res3 = pool.apply_async(f, (1000,))
        res4 = pool.apply_async(f, (1000,))
        print(res1.get(timeout=10))
        print(res2.get(timeout=10))
        print(res3.get(timeout=10))
        print(res4.get(timeout=10))
```
