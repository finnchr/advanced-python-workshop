# Decorators

Koble funksjon til http request

```
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"
```

Endre oppførsel til funksjon

```
@log_entry
@log_exit
def add(a, b)
	print(a + b)

>>> add(2, 3)
Enter 'add'
5
Leaving 'add'
```

Note:
To eksempler på bruk av dekorator
- Koble funksjon til request i Flask
- Endre oppførselen til en funksjon; logge før og etter funskjonskall

---

## Decorator wrapper funksjon

```
@log_entry
@log_exit
def add(a, b)
	print(a + b)
```

er det samme som

```
a = log_exit(add)
b = log_entry(a)
add = b
```

Note:
* En dekorator er i bunn og grunn bare en funksjon som wrapper en annen funksjon
* Dekorator er ansvarlig for at wrappet funksjon faktisk blir kalt
* Dekorator kan like greit sørge for at wrappet funksjon ikke blir kalt

---

## Enkel decorator

```
def my_decorator(func):
    def wrapper_func():
        print("--> Enter my decorator")
        func()
        print("<-- Leaving my decorator")

    return wrapper_func

@my_decorator
def hello_world():
    print("Hello world!")

>>> hello_world()
--> Enter my decorator
Hello world!
<-- Leaving my decorator
```

Note:
Her ser vi en veldig enkel dekorator.

Dekorator i eksempel gir ikke anledning til at dekorert funksjon kan ta noen argumenter. Det kan være en ugrei begrensning.

---

## Decorator for funksjon som tar argumenter

```
def my_decorator(func):
    def wrapper_func(arg1):
        print("--> Enter my decorator")
        func(arg1)
        print("<-- Leaving my decorator")

    return wrapper_func

@my_decorator
def hello_world(print_func):
    print_func("Hello world!")

>>> hello_world(print)
--> Enter my decorator
Hello world!
<-- Leaving my decorator
```

Note:
Dekorert funksjon kan ta ett argument. Bedre enn ingen, men fremdeles en litt kjedelig begrensning.

---

## Decorator for funksjon med vilkårlig antall argumenter

```
def my_decorator(func):
    def wrapper_func(*argc, **kwargs):
        print("--> Enter my decorator")
        func(*argc, **kwargs)
        print("<-- Leaving my decorator")

    return wrapper_func
```

Note:
* Her sier vi at dekorert funksjon kan ta i mot vilkårlig antall argumenter. Dekorator legger ingen begrensninger - og sånn bør det være
* Det er kanskje bare når dekorator brukes til å koble funksjon opp mot rammeverk at dekorator kan/bør legge begrensninger på antall argumenter

---

```
@my_decorator
def print_sum(a, b, c, d, e):
    print(a + b + c + d + e)

print_sum(2, 3, 4, 5, 6)
```

Note:
Vi kan ha så mange argumenter vi vil.

---

## Funksjonsnavn

```
@my_decorator
def print_sum(a, b):
    print(a+b)

@my_decorator
def print_max(a, b):
    print(a if a > b else b)

>>> print_sum.__name__
'wrapper_func'
>>> print_max.__name__
'wrapper_func'
```

Decorert funksjon blir skjult av wrapper-funksjon.

Note:
Når vi dekorerer en fuksjon får vi i realiteten en ny funksjon. I eksempel over ser vi at de to dekorerte funksjoner  ender opp med å få samme funksjonsnavn. Det kan føre til problemer; f.eks. bruker Flask funksjonsnavn til å tildele ruten en id.

---

```
def my_decorator(func):
    def wrapper_func(*argc, **kwargs):
        print("--> Enter my decorator")
        func(*argc, **kwargs)
        print("<-- Leaving my decorator")

    # kopierer funksjonsnavn
    wrapper_func.__name__ = func.__name__

    return wrapper_func
```

Note:
Her kopierer vi funksjonsnavnet til wrappet funksjon over på wrapper-funksjonen.

---

```
@my_decorator
def print_sum(a, b):
    print(a+b)

@my_decorator
def print_max(a, b):
    print(a if a > b else b)

>>> print_sum.__name__
'print_sum'
>>> print_max.__name__
'print_max'
```

---

### Den riktige måten!

```
from functools import wraps

def my_decorator(func):
    @wraps(func)
    def wrapper_func(*argc, **kwargs):
        print("--> Enter my decorator")
        func(*argc, **kwargs)
        print("<-- Leaving my decorator")

    return wrapper_func
```

Note:
* Istede for å manuelt kopiere funksjonnavn bruker vi heller standardbiblioteket sin funksjonalitet `@wraps`
* `@wraps` gjør litt ekstra (kopierer flere attributter)

Dette eksempelet kan brukes som templat til egne dekoratoer.
