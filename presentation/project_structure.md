## Project structure

```
/path/to/worlddomination
├─ requirements.txt
├─ worlddomination
│  ├─ __init__.py
│  ├─ __main__.py
│  ├─ core.py
│  ├─ europa
│  │  ├─ __init__.py
│  │  ├─ norway.py
│  │  └─ sweden.py
│  ├─ asia
│  │  ├─ __init__.py
│  │  ├─ japan.py
│  │  └─ china.py
└─ tests
   ├─ worlddomination_core.py
   ├─ worlddomination_europa_norway.py
   ├─ worlddomination_asia_japan.py
   └─ ...
```

---

### Alternativt

```
/path/to/worlddomination
├─ requirements.txt
├─ worlddomination
│  ├─ __init__.py
│  └─ ...
└─ tests
   └─ worlddomination
      ├─ core.py
      ├─ europa
      │  ├─ norway.py
      │  └─ ...
      ├─ asia
      │  ├─ japan.py
      │  └─ ...
```

Note:
Dette er alternativer til hvordan et prosjekt kan struktureres, men ikke nødvendigvis en fasit. Det kan være gode grunner til å velge andre strukturer - dette er ment som et godt utgangspunkt.
