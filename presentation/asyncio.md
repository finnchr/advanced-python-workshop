# Asyncio

> This module provides infrastructure for writing single-threaded concurrent code using coroutines, multiplexing I/O access over sockets and other resources, running network clients and servers, and other related primitives.

Note:
Det er vanskelig å gi en god norsk oversettelse av beskrivelsen ...

---

## Eksempel

```
import asyncio

async def compute(n):
    await asyncio.sleep(3)
    return n*n

async def test_await():
    res = await compute(10)
    print(res)

async def test_future():
    coroutine = compute(10)

    def result_ready(future):
        print("Callback: {0}".format(future.result()))

    future = asyncio.ensure_future(coroutine)
    future.add_done_callback(result_ready)

    result = await future
    print(result)


loop = asyncio.get_event_loop()
loop.run_until_complete(test_await())
loop.run_until_complete(test_future())
loop.close()
```
