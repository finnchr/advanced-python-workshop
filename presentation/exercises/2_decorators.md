## Decorators

### Oppgave 1

Lag decorator `debug` som logger følgende informasjon

* funksjonsnavn
* argumenter med verdier

---

### Oppgave 2

Utvid decorator slik at du kan angi funksjon decorator skal bruke til å skrive informasjonen

```
@debug(print)
def some_function(...):
    pass

@debug(logger.info)
def another_function(...):
    pass
```

<br/>

<small>
<strong>Hint:</strong>
Decorator må ha to nivåer; første nivå tar i mot funksjon som angir hvordan informasjon skal skrives,
andre nivå tar i mot funksjonen som skal dekoreres. Mao., du må lage et nivå "over" decorator i første oppgave.
</small>

<small>
En naiv implementasjon vil kunne se slik ut:
</small>

```
def some_function(...):
    pass

some_function = debug(print)(some_function)

```

---

### Oppgave 3

Skrive decorator som kobler funksjon til en hendelse

Vi ønsker å kunne skrive følgende:
```
bus = EventBus()

@bus.onevent("started")
def handle_started(event_name, event_data):
    print("Handle event started: {0}".format(event_data))

@bus.onevent("stopped")
def handle_stopped(event_name, event_data):
    print("Handle event stopped: {0}".format(event_data))

@bus.onevent("started")
@bus.onevent("stopped")
def handle_all_events(event_name, event_data):
    print("Another event handler")
    print("   event_name = {0}".format(event_name))
    print("   event_data = {0}".format(event_data))

bus.publish("started", {})
bus.publish("stopped", {})
```

<br/>

<small>
<strong>Hint:</strong>
Implementer decorator-funksjon i en klasse.
</small>
