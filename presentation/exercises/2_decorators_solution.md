# Decorators - oppgaver

## Oppgave 1 - løsning:
```
from functools import wraps

def debug(func):
    @wraps(func)
    def wrapper_func(*argc, **kwargs):
        logger_func("'{0}' invoked with the following arguments:".format(func.__name__))
        logger_func(argc)
        logger_func(kwargs)
        return func(*argc, **kwargs)

    return wrapper_func
```

## Oppgave 2 - løsning

```
from functools import wraps

def debug(logger_func):
    def inner_debug(func):
        @wraps(func)
        def wrapper_func(*argc, **kwargs):
            logger_func("'{0}' invoked with the following arguments:".format(func.__name__))
            logger_func(argc)
            logger_func(kwargs)

            return func(*argc, **kwargs)

        return wrapper_func

    return inner_debug
```

## Oppgave 3 - løsning

```
from collections import defaultdict

class EventBus:
    def __init__(self):
        self._handlers = defaultdict(list)

    def onevent(self, event_name):
        def _inner_onevent(func):
            self._handlers[event_name].append(func)
            return func

        return _inner_onevent

    def publish(self, event_name, event_data):
        for handler in self._handlers.get(event_name, []):
            handler(event_name, event_data)
```
