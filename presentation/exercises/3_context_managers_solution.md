# Context manager

## Oppgave 1 - løsning

```
import contextlib

@contextlib.contextmanager
def get_resource():
    resource = Resource()
    try:
        yield resource
    finally:
        resource.close()


with get_resource() as resource:
    resource.dostuff()
```

### Alternativ løsning

Implementer `__enter__` og `__exit__`

```
class Resource:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
```

## Oppgave 2 - løsning


Vi kan bruke metoden `closing`

```
from contextlib import closing

with closing(AllocateResource()) as resource:
    resource.dostuff()
```
