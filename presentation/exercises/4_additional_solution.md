# Ekstra: Bruk av standardbibliotek

## Oppgave 1 - løsning

```
def union(left, right):
    return sorted(set(a+b))
```

## Oppgave 2 - løsning

### Løsning 1

```
from functools import lru_cache

@lru_cache(100)
def fib(n):
    if n <= 0:
       return 0
    elif n == 1 or n == 2:
        return 1
    else:
        return fib(n-2) + fib(n-1)
```

### Løsning 2

```
class Fibonacci:
    def __init__(self):
        self._cache = [0, 1, 1]

    def __call__(self, n):
        if len(self._cache) > n:
            return self._cache[n]
        else:
            self._cache.append(self(n-2) + self(n-1))

        return self(n)

fib = Fibonacci()
```

## Oppgave 3 - løsning

### Løsning 1

```
sum([fib(n) for n in range(0, 100)])
```

### Løsning 2

```
from functools import reduce
reduce(lambda a,b: a+b, [fib(n) for n in range(0, 100)])
```

## Oppgave 4 - løsning

```
index = round(len(numbers)/2)
first_half = sum(numbers[:index])
second_half = sum(numbers[index:])
```

## Oppgave 5. Finn summen av to vektorer

Vi ønsker å skrive `result = vector_a + vector_b`

## 6. Finn kryss-produktet mellom to vektorer

Vi ønsker å skrive `result = vector_a * vector_b`

## 7. Skaler vektor

Vi ønsker å skrive `result = vector_a * 10`

### Løsning på oppgave 5, 6 og 7:

```
def __add__(self, right):
    if isinstance(right, Vector):
        return Vector(
          self.x + right.x,
          self.y + right.y,
          self.z + right.z
        )
    else:
        raise Exception()

def __mul__(self, right):
    if isinstance(right, Vector):
        return Vector(
            self.y*right.z - self.z*right.y,
            self.z*right.x - self.x*right.z,
            self.x*right.y - self.y*right.x
        )
    elif isinstance(right, int) or isinstance(right, float):
        return Vector(
            self.x*right,
            self.y*right,
            self.z*right
        )
    else:
        raise Exception()
```

# CSV-filer

## Oppgave 8 - løsning

```
import json

with open("people.json", "w") as jsonfile:
    jsonfile.write(json.dumps(list(get_values()), indent=4))
```

## Oppgave 9 - løsning

```
import json

with open("people.json", "r") as fh:
    people = json.load(fh)
    print("\n".join([p["name"] for p in people if int(p["age"]) > 18]))
```

## Oppgave 10 - løsning

```
def people_hook(p):
    return Person(p["name"], int(p["age"]))

with open("people1.json", "r") as fh:
    people = json.load(fh, object_hook=people_hook)
    print(people)
    print("\n".join([p.name for p in people if p.age > 18]))
```
