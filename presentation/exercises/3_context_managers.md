## Context manager

Gitt klassen

```
class AllocateResource:
    reference_count = 0

    def __init__(self):
        AllocateResource.reference_count += 1
        self._initialized = True

    def close(self):
        self._initialized = False
        AllocateResource.reference_count -= 1

    def dostuff(self):
        if not self._initialized:
            raise Error("Resouce not initialized")
```

---

### Oppgave 1

Skriv context manager som erstater følgende kode:

```
def dostuff():
    resource = AllocateResource()
    try:
        resource.dostuff()
    finally:
        resource.close()
```

---

### Oppgave 2

Er det annen funksjonalitet i contextlib vi kan bruke?
