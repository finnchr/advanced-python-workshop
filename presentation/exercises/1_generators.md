## Generators and list comprehension        

Gitt

```
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
```

### Oppgave 1

Lag ny liste hvor alle tallene er multiplisert med 10

```
[1, 2, 3, 4] => [10, 20, 30, 40]
```

### Oppgave 2

Lag ny liste med verdiene True og False

* Erstatt heltall med True
* Erstatt oddetall med False

```
[1, 2, 3, 4] => [False, True, False, True]
```

### Oppgave 3

Filtrer listen slik at du sitter igjen med kun partall

```
[1, 2, 3, 4] => [2, 4]
```
