# Generaors and list comprehension

Gitt `numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]`

## Oppgave 1 - løsning

```
[e*10 for e in numbers]
```

## Oppgave 2 - løsning

```
[e%2==0 for e in numbers]
```

## Oppgave 3 - løsning

```
[e for e in numbers if e%2 == 0]
```
