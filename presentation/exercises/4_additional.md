## Ekstra: Bruk av standardbibliotek

---

### Oppgave 1

Smelt sammen to lister

Gitt to lister a og b. Skriv funksjon som gir unionen av list a og list b.

```
def union(left, right):
    ...

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 21]

def test_union():
    c = union(a, b)
    print(c)
    assert c == [1,2,3,4,5,6,7,8,13,21,34,55,89]
```

---

### Opppgave 2

Hvor raskt klarer du å finne de 100 første fibonacci-tallene?

For å ta tiden kan du brukt timeit-modulen. F.eks. dersom du lager funksjonen `fib` i filen `fibonacci.py`, kan du ta tiden med følgende kommando:
```
$ python3 -m timeit "from fibonacci import fib; fib(10)"
```

### Funksjon som finner fibonacci-tall på posisjon n

```
def fib(n):
    if n == 0:
       return 0
    elif n == 1 or n == 2:
        return 1
    else:
        return fib(n-2) + fib(n-1)
```

### Oppgave 3

Finn summen av de N første fibonacci-tallene

### Oppgave 4

Del opp en list med tall.

Gitt tilfeldig liste med tall: Finn summen av tallene som utgjør første og siste halvdel av listen.

For listen `[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]` vil det bli:
```
first_half = sum(1,2,3,4,5)
second_hald = sum(6,7,8,9,10)
```

---

## Operatorer

Gitt klassen

```
class Vector:
    def __init__(self, x, y, z):
        self._vec = [x, y, z]

    @property
    def x(self):
        return self._vec[0]

    @property
    def y(self):
        return self._vec[1]

    @property
    def z(self):
        return self._vec[2]

```
---

### Oppgave 5

Finn summen av to vektorer.

Vi ønsker å skrive `result = vector_a + vector_b`

### Oppgave 6

Finn kryss-produktet mellom to vektorer.

Vi ønsker å skrive `result = vector_a * vector_b`

### Oppgave 7

Skaler vektor.

Vi ønsker å skrive `result = vector_a * 10`

---

Tester som verifiserer løsningen:

```
def test_add():
    a = Vector(3,6,7)
    b = Vector(2,11,13)
    c = a + b

    assert c.x == a.x + b.x
    assert c.y == a.y + b.y
    assert c.z == a.z + b.z

def test_cross_product():
    a = Vector(1,2,3)
    b = Vector(-2,9,7)
    c = a * b

    assert c.x == a.y*b.z - a.z*b.y
    assert c.y == a.z*b.x - a.x*b.z
    assert c.z == a.x*b.y - a.y*b.x

def test_scalar():
    a = Vector(3,5,9)
    c = a * 3

    assert c.x == a.x * 3
    assert c.y == a.y * 3
    assert c.z == a.z * 3
```

## CSV-filer

Gitt filen `people.csv`
```
id;name;age
1;Alice;37
2;Bob;49
3;Trudy;17
4;Ada;87
5;Earl;23
6;Lisa;3
7;Travis;8
```

---

To måter å lese en CSV-fil på:

```
import csv

def get_values():
    with open("people.csv", newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=";", quotechar='"')
        headers = next(reader)
        rows = []
        for values in reader:
            row = {}
            for index,header in enumerate(headers):
                row[header] = values[index]
            yield row
```

```
def get_values():
    with open("people.csv", newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        rows = []
        for row in reader:
            yield row
```

---

### Oppgave 8

Skriv funksjon som leser CSV-filen og skriver den til ny en fil som json.

---

## JSON-filer

Gitt filen `people.json`

```
[
    {
        "id": "1",
        "age": "37",
        "name": "Alice"
    },{
        "id": "2",
        "age": "49",
        "name": "Bob"
    },{
        "id": "3",
        "age": "17",
        "name": "Trudy"
    },{
        "id": "4",
        "age": "87",
        "name": "Ada"
    },{
        "id": "5",
        "age": "23",
        "name": "Earl"
    },{
        "id": "6",
        "age": "3",
        "name": "Lisa"
    },{
        "id": "7",
        "age": "8",
        "name": "Travis"
    }
]
```

---

### Oppgave 9

Les inn filen og skriv ut navnet på alle hvor `age >= 18`. Bruk gjerne `print` for å skrive ut navnet.

---

## namedtuple

Lettvektsobjekter som er lette å opprette

```
from collections import namedtuple

from collections import namedtuple
Person = namedtuple('Person', 'name age')

p = Person("Bob", 44)

assert p.name == "Bob"
assert p.age == 44
```

### Oppgave 10

Skriv om løsningen i oppgave 9 slik at følgende fungerer:

```
from collections import namedtuple

Person = namedtuple('Person', 'name age')

with open("people.json", "r") as fh:
    people = json.load(fh)
    print("\n".join([p.name for p in people if p.age >= 18]))
```
