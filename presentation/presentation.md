# Python workshop

---

# Innhold

* Environment
* Project structure
* Moduler
* Testing
* Tools
* Funksjoner
* Generator og list comprehension
* Decorators
* Context managers
* Special functions
* Standard library
* PEP8
* Pythonic code
* Oppgaver
