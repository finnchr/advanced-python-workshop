# Standard Library

* Omfattende
* Innebygde funksjoner og moduler
* Moduler som løser "hverdagsproblemer"
* Exceptions
* Gode eksempler på bruk

Note:
Standardbibliotek er ganske omfattende og inneholder mange moduler som kommer godt med i ulike situasjoner.

Det er mange skjulte skatter i standardbiblioteket.
