from collections import namedtuple

Person = namedtuple('Person', 'name age')

def read_people():
    with open("people.json", "r") as fh:
        return json.load(fh)

def test_namedtuple():
    people = read_people()

    adults = list(p.name for p in people if p.age >= 18)
    assert len(adults) == 4
