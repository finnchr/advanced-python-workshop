import csv

"""
def get_values():
    with open("people.csv", newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=";", quotechar='"')
        headers = next(reader)
        rows = []
        for values in reader:
            row = {}
            for index,header in enumerate(headers):
                row[header] = values[index]
            yield row
"""

def get_values():
    with open("people.csv", newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        rows = []
        for row in reader:
            yield row
