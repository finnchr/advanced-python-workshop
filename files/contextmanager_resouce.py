class AllocateResource:
    reference_count = 0

    def __init__(self):
        AllocateResource.reference_count += 1
        self._initialized = True

    def close(self):
        self._initialized = False
        AllocateResource.reference_count -= 1

    def dostuff(self):
        if not self._initialized:
            raise Error("Resouce not initialized")
