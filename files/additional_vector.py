class Vector:
    def __init__(self, x, y, z):
        self._vec = [x, y, z]

    @property
    def x(self):
        return self._vec[0]

    @property
    def y(self):
        return self._vec[1]

    @property
    def z(self):
        return self._vec[2]



def test_add():
    a = Vector(3,6,7)
    b = Vector(2,11,13)
    c = a + b

    assert c.x == a.x + b.x
    assert c.y == a.y + b.y
    assert c.z == a.z + b.z

def test_cross_product():
    a = Vector(1,2,3)
    b = Vector(-2,9,7)
    c = a * b

    assert c.x == a.y*b.z - a.z*b.y
    assert c.y == a.z*b.x - a.x*b.z
    assert c.z == a.x*b.y - a.y*b.x

def test_scalar():
    a = Vector(3,5,9)
    c = a * 3

    assert c.x == a.x * 3
    assert c.y == a.y * 3
    assert c.z == a.z * 3
