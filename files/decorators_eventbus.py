class EventBus():
    def __init__(self):
        pass

    def onevent(event_name):
        pass

    def publish(event_name, event_data):
        pass


bus = EventBus()

@bus.onevent("started")
def handle_started(event_name, event_data):
    print("Handle event started: {0}".format(event_data))

@bus.onevent("stopped")
def handle_stopped(event_name, event_data):
    print("Handle event stopped: {0}".format(event_data))

@bus.onevent("started")
@bus.onevent("stopped")
def handle_all_events(event_name, event_data):
    print("Another event handler")
    print("   event_name = {0}".format(event_name))
    print("   event_data = {0}".format(event_data))

bus.publish("started", {})
bus.publish("stopped", {})
