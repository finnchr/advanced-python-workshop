PRESENTATION_PATH=$(CURDIR)/presentation
PRESENTATION=$(PRESENTATION_PATH)/presentation.md

VENV=.venv
PIP=$(VENV)/bin/pip
ENV=dev
REQUIREMENTS=requirements-$(ENV).txt

COVERAGE=$(VENV)/bin/coverage
FLAKE8=$(VENV)/bin/flake8
PYLINT=$(VENV)/bin/pylint

present:
	@mkdir -p $(PRESENTATION_PATH)
	@mkdir -p $(PRESENTATION_PATH)/media
	@mkdir -p $(PRESENTATION_PATH)/menu

	@docker run -it --rm -p 8000:8000 \
	--net host \
	-v $(PRESENTATION_PATH)/index.html:/reveal.js/index.html \
	-v $(PRESENTATION_PATH)/custom.css:/reveal.js/css/theme/custom.css \
	-v $(PRESENTATION_PATH)/media:/reveal.js/media \
	-v $(PRESENTATION_PATH)/menu:/reveal.js/plugin/menu \
	-v $(PRESENTATION_PATH)/presentation.md:/reveal.js/presentation.md \
	-v $(PRESENTATION_PATH)/environment.md:/reveal.js/environment.md \
	-v $(PRESENTATION_PATH)/project_structure.md:/reveal.js/project_structure.md \
	-v $(PRESENTATION_PATH)/modules.md:/reveal.js/modules.md \
	-v $(PRESENTATION_PATH)/testing.md:/reveal.js/testing.md \
	-v $(PRESENTATION_PATH)/tools.md:/reveal.js/tools.md \
	-v $(PRESENTATION_PATH)/functions.md:/reveal.js/functions.md \
	-v $(PRESENTATION_PATH)/generators_and_list_comprehension.md:/reveal.js/generators_and_list_comprehension.md \
	-v $(PRESENTATION_PATH)/decorators.md:/reveal.js/decorators.md \
	-v $(PRESENTATION_PATH)/context_managers.md:/reveal.js/context_managers.md \
	-v $(PRESENTATION_PATH)/special_functions.md:/reveal.js/special_functions.md \
	-v $(PRESENTATION_PATH)/multiprocessing.md:/reveal.js/multiprocessing.md \
	-v $(PRESENTATION_PATH)/asyncio.md:/reveal.js/asyncio.md \
	-v $(PRESENTATION_PATH)/multiprocessing_vs_asyncio.md:/reveal.js/multiprocessing_vs_asyncio.md \
	-v $(PRESENTATION_PATH)/standard_library.md:/reveal.js/standard_library.md \
	-v $(PRESENTATION_PATH)/pep8.md:/reveal.js/pep8.md \
	-v $(PRESENTATION_PATH)/pythonic.md:/reveal.js/pythonic.md \
	-v $(PRESENTATION_PATH)/workshop_exercises.md:/reveal.js/workshop_exercises.md \
	-v $(PRESENTATION_PATH)/exercises/1_generators.md:/reveal.js/exercises/1_generators.md \
	-v $(PRESENTATION_PATH)/exercises/2_decorators.md:/reveal.js/exercises/2_decorators.md \
	-v $(PRESENTATION_PATH)/exercises/3_context_managers.md:/reveal.js/exercises/3_context_managers.md \
	-v $(PRESENTATION_PATH)/exercises/4_additional.md:/reveal.js/exercises/4_additional.md \
	--name=python-workshop-presentation \
	nbrown/revealjs


clean:
	rm -rf $(VENV)

$(VENV):
	virtualenv --python=python3 $(VENV)

bootstrap: $(VENV)
	$(PIP) install -r $(REQUIREMENTS)

test:
	$(VENV)/bin/pytest --verbosity=1

coverage:
	$(COVERAGE) run test.py

flake8:
	$(FLAKE8) --show-source --ignore W391 test.py

pylint:
	$(PYLINT) test.py
